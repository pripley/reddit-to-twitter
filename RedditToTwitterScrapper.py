import urllib3
from bs4 import BeautifulSoup
from bs4 import UnicodeDammit
from urllib3 import PoolManager
from datetime import datetime
import csv
import pyodbc
import pypyodbc
import configparser
from twitter import *
#import requests
#from requests.packages.urllib3.exceptions import InsecureRequestWarning

settings = configparser.ConfigParser()
settings._interpolation = configparser.ExtendedInterpolation()
settings.read('settings.ini')

OAUTH_TOKEN = settings.get("Twitter", "OAUTH_TOKEN")
OAUTH_SECRET = settings.get("Twitter", "OAUTH_SECRET")
CONSUMER_KEY = settings.get("Twitter", "CONSUMER_KEY")
CONSUMER_SECRET = settings.get("Twitter", "CONSUMER_SECRET")
DATABASE_CONNECTON_STRING = settings.get("Database", "DATABASE_CONNECTON_STRING")

# build list of reddit urls to search for stories
SEARCH_STRINGS = settings.get('SearchStrings', 'URLS').split('\n')

manager = PoolManager(2)
print("Starting")

# list for tracking urls found on each reddit page
links = []

# for each reddit page, collect links to articles
for searchString in SEARCH_STRINGS:
    r = manager.request('GET', searchString)
    soup = BeautifulSoup(r.data)
    links.extend(soup.find_all("a", class_="title may-blank outbound"))

try:
    print("try to connect")
    # connect to the database
    cnxn = pyodbc.connect(DATABASE_CONNECTON_STRING)
    cursor = cnxn.cursor()
    print("connected!")

    rows = []
    links = set(links)        
    #for each article link we find, filter 
    for link in links:
                hyperlink = link.get('href')
                ltext = link.get_text()

                # filter out any stories that are on reddit; we only want stories that are elsewhere on the web
                if hyperlink.startswith('http') == False : continue
                if hyperlink.startswith('http://www.reddit.com') : continue
                
				# filter out personal pronoun stories (since these stories wouldn't make sense to post in a Twitter feed)
                if " my " in ltext: continue
                if "My " in ltext: continue
                if " me " in ltext: continue
                if " I " in ltext: continue
                if " myself" in ltext: continue
                if ltext.startswith('I ') : continue
           
                fullURL = hyperlink
				# do we already have the article link in our database?
                cursor.execute("select count(*) as countrecs from tweetedLinks where fullURL = ?", fullURL)
                row = cursor.fetchone()
                
				# have we seen this article link before?  If yes, then skip this link.
                if row.countrecs > 0:
                    #print("seen you before!")
                    continue
                
				# shorten article hyperlink
                hyperlink = 'http://is.gd/create.php?format=simple&url=' + hyperlink
                
                t = manager.request('GET', hyperlink)
                
				# convert the shortened link to unicode
                shortenedUnicodeLink = UnicodeDammit(t.data).unicode_markup
                
                print(shortenedUnicodeLink.encode('ascii', 'ignore'))
                
                # calculate how many characters are available for the article title minus the article link length
                avail_tweet_len = 140 - len(shortenedUnicodeLink) - 2
                print(avail_tweet_len)
                
				# 
                trunc_link_text = (ltext[:avail_tweet_len] + '..') if len(ltext) > avail_tweet_len else ltext
                print(trunc_link_text.encode('ascii', 'ignore'))    
                
                # make a comma separated string for insertion into the database
                row = trunc_link_text + ',' + shortenedUnicodeLink + ','  + hyperlink + ',' + datetime.ctime(datetime.now()) + ',no'
                rows.append(row.encode('ascii', 'ignore'))
                cursor.execute("insert into tweetedlinks(fullURL, shortenedURL, headline) values (?, ?, ?)", fullURL, shortenedUnicodeLink, trunc_link_text)
                cnxn.commit()

            

except (IndexError):
            pass


# get most recent non-tweeted link
cursor.execute("select id, fullURL, shortenedURL, headline from dbo.tweetedLinks where tweeted = 0 order by collectionDate DESC")
linktotweet = cursor.fetchone()

# check that we found an article in the database that we haven't tweeted yet
if linktotweet == None:
    print("no new links to tweet... exiting")
    exit
linktotweetID = linktotweet.id

# post article to twitter
t = Twitter(
            auth=OAuth(OAUTH_TOKEN, OAUTH_SECRET,
                       CONSUMER_KEY, CONSUMER_SECRET)
           )
availableHeadlineLength = 140 - 1 - len(linktotweet.shortenedURL)

# shorten the title so that both it and the link to the article fit in the tweet
shortenedHeadline = linktotweet.headline[0:availableHeadlineLength]

# build tweet string                
tweetStr = shortenedHeadline.strip() + " " + linktotweet.shortenedURL.strip()

print(tweetStr)
t.statuses.update(
    status=tweetStr)

#update the tweeted link's record to mark it as tweeted (and the date of tweeting)
cursor.execute("update dbo.tweetedLinks set tweeted = 1, tweetedDate = GETDATE() where id = ?", linktotweetID)
cnxn.commit()

cnxn.close()
print("Completed successfully")



